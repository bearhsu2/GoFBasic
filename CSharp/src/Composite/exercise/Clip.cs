﻿/*
 * Copyright 2017 TeddySoft Technology. 
 *
 */
using System;
using System.Collections.Generic;

namespace Tw.Teddysoft.Gof.Composite.Exercise
{
    public class Clip
	{
        IList<Clip> clips = new List<Clip>();
        IList<Bullet> bullets = new List<Bullet>();

        public bool addClip(Clip clip)
        {
            clips.Add(clip);
            return true;
        }
        public bool removeClip(Clip clip)
        {
            clips.Remove(clip);
            return true;
        }
        public Clip getClip(int index)
        {
            return clips[index];
        }
        public bool addBullet(Bullet bullet)
        {
            bullets.Add(bullet);
            return true;
        }
        public bool removeBullet(Bullet bullet)
        {
            bullets.Remove(bullet);
            return true;
        }
        public Bullet getBullet(int index)
        {
            return bullets[index];
        }
        public void fire()
        {
            foreach (Bullet bullet in bullets)
            {
                bullet.fire();
            }
            foreach (Clip clip in clips)
            {
                clip.fire();
            }
        }
    }
}
