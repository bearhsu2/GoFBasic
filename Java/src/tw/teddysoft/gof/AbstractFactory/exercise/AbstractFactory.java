package tw.teddysoft.gof.AbstractFactory.exercise;

public abstract class AbstractFactory {
    public abstract Drive createDrive(String type, int index);
    public abstract Process createProcess(int id);
    public abstract IOPort createIOPort(int address);
    public abstract Monitor createMonitor(int id);
}
