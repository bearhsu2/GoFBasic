package tw.teddysoft.gof.AbstractFactory.exercise;

public class LinuxFactory extends AbstractFactory {
    @Override
    public Drive createDrive(String type, int index) {
        return new SimpleDriveFactory().createLinuxDrive(type, index);
    }


    @Override
    public Process createProcess(int id) {
        return new LinuxProcess(id);
    }


    @Override
    public IOPort createIOPort(int address) {

        return new LinuxIOPort(address);
    }


    @Override
    public Monitor createMonitor(int id) {
        return new LinuxMonitor(id);
    }
}
