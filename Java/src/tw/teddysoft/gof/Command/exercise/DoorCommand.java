package tw.teddysoft.gof.Command.exercise;

public class DoorCommand implements Command {
    private Door door;


    public DoorCommand(Door door) {

        this.door = door;
    }


    @Override
    public Result execute() {

        if (this.door.getDoorStatus().equals("open")) {
            return new Result(Status.CRITICAL, "門被開啟");
        }
        return new Result(Status.OK);
    }
}
