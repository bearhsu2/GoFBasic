package tw.teddysoft.gof.Command.exercise;

public class ThermalCommand implements Command {

    Thermal thermal;


    public ThermalCommand(Thermal thermal) {
        this.thermal = thermal;
    }


    @Override
    public Result execute() {
        if (this.thermal.isOverheat()) {
            Result result = new Result(Status.CRITICAL);
            result.setMessage("溫度過熱");
            return result;
        }
        return new Result(Status.OK);
    }
}
