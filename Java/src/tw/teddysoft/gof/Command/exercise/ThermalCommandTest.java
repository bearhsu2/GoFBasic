package tw.teddysoft.gof.Command.exercise;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ThermalCommandTest {

    @Test
    public void When_NOT_OverHeat_Then_OK() {

        Thermal thermal = new DummyThermal("1.2.3.4", false);

        Command command = new ThermalCommand(thermal);


        Result result = command.execute();
        assertEquals(Status.OK, result.getStatus());
        assertTrue(result.getMessage().isEmpty());


    }


    @Test
    public void When_OverHeat_Then_Critical() {

        Thermal thermal = new DummyThermal("1.2.3.4", true);

        Command command = new ThermalCommand(thermal);


        Result result = command.execute();
        assertEquals(Status.CRITICAL, result.getStatus());
        assertTrue(result.getMessage().startsWith("溫度過熱"));


    }

    class DummyThermal extends Thermal {

        boolean expectedOverheat;


        public DummyThermal(String ipAddress, boolean expectedOverheat) {
            super(ipAddress);
            this.expectedOverheat = expectedOverheat;
        }


        @Override
        public boolean isOverheat() {
            return this.expectedOverheat;
        }
    }

}