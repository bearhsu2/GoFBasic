package tw.teddysoft.gof.Command.exercise;

public class WindowCommand implements Command {
    private final Window window;


    public WindowCommand(Window window) {
        this.window = window;
    }


    @Override
    public Result execute() {

        if (this.window.isBroken()) {
            return new Result(Status.CRITICAL, "窗戶破壞");
        }
        if (this.window.isOpen()) {
            return new Result(Status.CRITICAL, "窗戶開啟");

        }
        return new Result(Status.OK);
    }
}
