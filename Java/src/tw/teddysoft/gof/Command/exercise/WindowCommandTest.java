package tw.teddysoft.gof.Command.exercise;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class WindowCommandTest {

    @Test
    public void When_NOT_Broken_Or_Open_Then_OK() {

        Window dummyWindow = new DummyWindow("1.2.3.4", false, false);
        Command command = new WindowCommand(dummyWindow);

        Result result = command.execute();
        assertEquals(Status.OK, result.getStatus());
        assertTrue(result.getMessage().isEmpty());


    }


    class DummyWindow extends Window {

        boolean expectedOpen;
        boolean expectedBroken;


        public DummyWindow(String ipAddress, boolean expectedOpen, boolean expectedBroken) {
            super(ipAddress);
            this.expectedOpen = expectedOpen;
            this.expectedBroken = expectedBroken;
        }

        @Override
        public boolean isBroken() {
            return this.expectedBroken;
        }


        @Override
        public boolean isOpen() {
            return this.expectedOpen;
        }
    }

}