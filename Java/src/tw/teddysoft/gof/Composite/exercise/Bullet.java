package tw.teddysoft.gof.Composite.exercise;

public abstract class Bullet extends Weapon{
	public abstract void fire();
}
