/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.Composite.exercise;

public class TraceBullet  extends Bullet {
	@Override
	public boolean add(Weapon weapon) {
		return false;
	}


	@Override
	public boolean remove(Weapon weapon) {
		return false;
	}


	@Override
	public Weapon getChild(int index) {
		return null;
	}


	@Override
	public void fire() {
		System.out.println("發射追蹤子彈.");
	}
}
