/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 *
 */
package tw.teddysoft.gof.FactoryMethod.exercise;

public abstract class DriveManager {

    abstract Drive getDrive(String type, int index);
}
