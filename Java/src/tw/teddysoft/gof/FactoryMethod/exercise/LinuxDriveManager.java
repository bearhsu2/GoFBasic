package tw.teddysoft.gof.FactoryMethod.exercise;


public class LinuxDriveManager extends DriveManager {
    @Override
    Drive getDrive(String type, int index) {


        switch (type) {
            case "SATA":
                return new LinuxSATADrive(index);
            case "USB":
                return new LinuxUSBDrive(index);
            default:
                throw new RuntimeException
                        ("Unsupported drive type: " + type);
        }


    }
}
