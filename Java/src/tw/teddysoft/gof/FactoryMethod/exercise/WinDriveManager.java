package tw.teddysoft.gof.FactoryMethod.exercise;

public class WinDriveManager extends DriveManager {
    @Override
    Drive getDrive(String type, int index) {
        switch (type) {
            case "SATA":
                return new WinSATADrive(index);
            case "USB":
                return new WinUSBDrive(index);
            default:
                throw new RuntimeException
                        ("Unsupported drive type: " + type);

        }
    }
}
