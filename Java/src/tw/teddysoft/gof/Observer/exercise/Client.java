package tw.teddysoft.gof.Observer.exercise;

import java.util.ArrayList;
import java.util.List;

public class Client implements Subject {
    private Command checkCommand;
    private Result result = new Result(Status.PENDING);

    private List<Observer> observers = new ArrayList<>();


    public Client(Command command) {
        this.checkCommand = command;
    }


    public Result getResult() {
        return result;
    }


    public void setResult(Result result) {
        this.result = result;
        notifyObserver();
    }



    public Command getCheckCommand() {
        return checkCommand;
    }


    public void setCheckCommand(Command command) {
        this.checkCommand = command;
    }


    public void addObserver(Observer observer) {
        this.observers.add(observer);
    }


    @Override
    public void deleteObserver(Observer observer) {
        this.observers.remove(observer);
    }


    @Override
    public void notifyObserver() {
        for (Observer observer : this.observers) {
            observer.update(this);
        }
    }
}
