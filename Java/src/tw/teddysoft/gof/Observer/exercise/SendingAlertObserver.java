package tw.teddysoft.gof.Observer.exercise;

public class SendingAlertObserver implements Observer {
    @Override
    public void update(Subject subject) {

        Client client = (Client) subject;

        if (client.getResult().getStatus() != Status.OK) {

            System.out.println("發現問題並通知保全人員: " + client.getResult().getMessage());
        }
    }
}
