/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.Strategy.exercise;

public interface Formater {
	void execute();
}
