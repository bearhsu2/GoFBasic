/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 *
 */
package tw.teddysoft.gof.TemplateMethod.exercise;

public abstract class ConfigParser {
    PersonData pData = null;


    public final PersonData doParse() {
        readConfig();
        parseToken();
        pData = buildModel();
        validate();
        return pData;
    }


    abstract protected void readConfig();


    abstract protected void parseToken();


    protected abstract PersonData buildModel();


    protected abstract void validate();
}
