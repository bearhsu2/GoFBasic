package tw.teddysoft.gof.TemplateMethod.exercise;

public class DbConfigParser extends ConfigParser {


    protected String connStr;


    public DbConfigParser(String connStr) {
        this.connStr = connStr;
    }


    @Override
    protected void readConfig() {
        System.out.println("Read config data "
                + "from database: " + this.connStr);
    }


    @Override
    protected void parseToken() {
        System.out.println("parseToken...");
    }


    @Override
    protected PersonData buildModel() {
        pData = new PersonData();
        pData.setHP(100);
        pData.setName("Kay");
        return this.pData;
    }


    @Override
    protected void validate() {
        System.out.println("validate config data built from database...");
    }


}
