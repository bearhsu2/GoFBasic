package tw.teddysoft.gof.TemplateMethod.exercise;

public class FileConfigParser extends ConfigParser {


    private final String fileName;


    public FileConfigParser(String s) {
        fileName = s;
    }


    @Override
    protected void readConfig() {
        System.out.println("Read config data from file: " + this.fileName);
    }


    @Override
    protected void parseToken() {
        System.out.println("parseToken...");
    }


    @Override
    protected PersonData buildModel() {
        this.pData = new PersonData();
        pData.setName("Teddy");
        pData.setHP(100);
        return this.pData;
    }


    @Override
    protected void validate() {

        System.out.println("validate config data built from file...");
    }
}
